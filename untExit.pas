unit untExit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, jpeg, ComCtrls, Gauges, MPlayer, StdCtrls,
  ActnList;

type
  TfrmExit = class(TForm)
    imgMainExit: TImage;
    tmrExit: TTimer;
    mplExit: TMediaPlayer;
    imgCancelDn: TImage;
    imgExitDn: TImage;
    lblExit: TLabel;
    lblCancel: TLabel;
    ActionList1: TActionList;
    actCancel: TAction;
    actClose: TAction;
    imgTrackbarExit: TImage;
    procedure tmrExitTimer(Sender: TObject);
    procedure lblCancelMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblCancelMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblExitMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblExitMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblCancelClick(Sender: TObject);
    procedure lblExitClick(Sender: TObject);
    procedure actCancelExecute(Sender: TObject);
    procedure actCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure CancelProc;
    procedure CloseProc;
    { Private declarations }
  public
    { Public declarations }
  end;

const
  TRACKBAR_SIZE = 290;

var
  frmExit: TfrmExit;

implementation

uses untMain;

{$R *.dfm}

procedure TfrmExit.tmrExitTimer(Sender: TObject);
var
  cp, l, t: Longint; // Current Position, Length
begin
  cp := mplExit.Position;// div 1000;
  l := mplExit.Length;// div 1000;
  t := cp * TRACKBAR_SIZE div l;
//  if (t mod 5) = 0 then
  if not (t >= TRACKBAR_SIZE - 1) then
    t := (t div 5) * 5;
  imgTrackbarExit.Width := t;
//  pgb.Position := mplExit.Position div 1000;
//  if pgb.Position >= pgb.Max then
  if imgTrackbarExit.Width >= TRACKBAR_SIZE then
    CloseProc;
end;

procedure TfrmExit.lblCancelMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  imgCancelDn.Visible := True;
end;

procedure TfrmExit.lblCancelMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  imgCancelDn.Visible := False;
end;

procedure TfrmExit.lblExitMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  imgExitDn.Visible := True;
end;

procedure TfrmExit.lblExitMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  imgExitDn.Visible := False;
end;

procedure TfrmExit.lblCancelClick(Sender: TObject);
begin
  CancelProc;
end;

procedure TfrmExit.lblExitClick(Sender: TObject);
begin
  CloseProc;
end;

procedure TfrmExit.actCancelExecute(Sender: TObject);
begin
  CancelProc;
end;

procedure TfrmExit.CancelProc;
begin
  tmrExit.Enabled := False;
  mplExit.Stop;
  mplExit.Close;
  Close;
end;

procedure TfrmExit.CloseProc;
begin
  Application.Terminate;
end;

procedure TfrmExit.actCloseExecute(Sender: TObject);
begin
  CloseProc;
end;

procedure TfrmExit.FormShow(Sender: TObject);
begin
//  pgb.Position := 0;
  imgTrackbarExit.Width := 0;
  tmrExit.Enabled := True;
  mplExit.Open;
  mplExit.Play;
end;

end.

unit untSplash;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, jpeg, ExtCtrls, ActnList, MPlayer, StdCtrls, Buttons;

type
  TfrmSplash = class(TForm)
    imgSplash: TImage;
    Timer1: TTimer;
    ActionList1: TActionList;
    actCloseSplash: TAction;
    MediaPlayer1: TMediaPlayer;
    imgVorood: TImage;
    lblVorood: TLabel;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure actCloseSplashExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lblVoroodMouseEnter(Sender: TObject);
    procedure lblVoroodMouseLeave(Sender: TObject);
    procedure lblVoroodMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    procedure Closing;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSplash: TfrmSplash;
  CloseVar: Boolean;

implementation

uses untMain;

{$R *.dfm}

procedure TfrmSplash.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose := CloseVar;
end;

procedure TfrmSplash.FormCreate(Sender: TObject);
begin
  CloseVar := False;
end;

procedure TfrmSplash.Timer1Timer(Sender: TObject);
begin
  Closing;
end;

procedure TfrmSplash.Closing;
begin
  CloseVar := True;
  Close;
  MediaPlayer1.Stop;
  MediaPlayer1.Close;
  Timer1.Enabled := False;
  frmMainNeiNava.Show;
  Free;
end;

procedure TfrmSplash.actCloseSplashExecute(Sender: TObject);
begin
  Closing;
end;

procedure TfrmSplash.FormShow(Sender: TObject);
begin
  MediaPlayer1.Open;
  MediaPlayer1.Play;
end;

procedure TfrmSplash.lblVoroodMouseEnter(Sender: TObject);
begin
  imgVorood.Visible := True;
end;

procedure TfrmSplash.lblVoroodMouseLeave(Sender: TObject);
begin
  imgVorood.Visible := False;
end;

procedure TfrmSplash.lblVoroodMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (X in [0..lblVorood.Width]) and
    (Y in [0..lblVorood.Height]) then
    Closing;
end;

end.

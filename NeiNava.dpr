program NeiNava;

uses
  Forms,
  untMain in 'untMain.pas' {frmMainNeiNava},
  untAbout in 'untAbout.pas' {frmAbout},
  untSplash in 'untSplash.pas' {frmSplash},
  untExit in 'untExit.pas' {frmExit};

{$R *.res}

begin
  Application.ShowMainForm := False;
  frmSplash := TfrmSplash.Create(Application);
  frmSplash.Show;
  frmSplash.Update;
  Application.Initialize;
  Application.Title := '�����';
  Application.CreateForm(TfrmMainNeiNava, frmMainNeiNava);
  Application.CreateForm(TfrmAbout, frmAbout);
  Application.CreateForm(TfrmExit, frmExit);
  Application.Run;
end.

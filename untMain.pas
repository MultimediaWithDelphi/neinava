unit untMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, MPlayer, Buttons, ComCtrls, VolCtrl, jpeg,
  ActnList;

type
  TfrmMainNeiNava = class(TForm)
    MediaPlayer1: TMediaPlayer;
    spbMute: TSpeedButton;
    Timer1: TTimer;
    lblLoop: TLabel;
    lblRepeat: TLabel;
    lblTime: TLabel;
    lblAh: TLabel;
    lblFk: TLabel;
    lblPlayPause: TLabel;
    lblStop: TLabel;
    imgPause: TImage;
    imgPauseO: TImage;
    imgPlay: TImage;
    imgLoop: TImage;
    lblTrackUser: TLabel;
    mVolControl1: TmVolControl;
    lblRewind: TLabel;
    lblForward: TLabel;
    lblNext: TLabel;
    lblPrevious: TLabel;
    imgMain: TImage;
    imgTrackMove: TImage;
    lblFullTime: TLabel;
    imgStop: TImage;
    imgRepeat: TImage;
    imgRewind: TImage;
    imgForward: TImage;
    imgNext: TImage;
    imgAh: TImage;
    imgKo: TImage;
    lblKo: TLabel;
    lblAg: TLabel;
    lblAv: TLabel;
    lblSo: TLabel;
    lblMu: TLabel;
    lblNa: TLabel;
    imgAg: TImage;
    imgFk: TImage;
    imgAv: TImage;
    imgSo: TImage;
    imgMu: TImage;
    imgNa: TImage;
    imgAbout: TImage;
    lblAbout: TLabel;
    imgMin: TImage;
    lblMin: TLabel;
    imgExit: TImage;
    lblExit: TLabel;
    imgPre: TImage;
    pnlMenuPanel: TPanel;
    imgMenuPanel: TImage;
    scb1: TScrollBox;
    imgsbKo: TImage;
    imgsbAh: TImage;
    imgsbAg: TImage;
    imgsbFk: TImage;
    imgsbAv: TImage;
    imgsbSo: TImage;
    imgsbMu: TImage;
    imgsbNa: TImage;
    imgsmAh: TImage;
    imgAh1: TImage;
    imgAh2: TImage;
    imgAh3: TImage;
    imgAh4: TImage;
    imgAh5: TImage;
    imgAh6: TImage;
    imgAh7: TImage;
    imgAh8: TImage;
    imgAh10: TImage;
    imgAh11: TImage;
    imgAh12: TImage;
    imgAh13: TImage;
    imgAh14: TImage;
    imgAh15: TImage;
    imgAh16: TImage;
    imgAh17: TImage;
    imgAh18: TImage;
    imgAh19: TImage;
    imgAh20: TImage;
    imgAh21: TImage;
    imgAh22: TImage;
    imgAh23: TImage;
    imgAh9: TImage;
    lblAh1: TLabel;
    lblAh13: TLabel;
    lblAh8: TLabel;
    lblAh7: TLabel;
    lblAh6: TLabel;
    lblAh5: TLabel;
    lblAh4: TLabel;
    lblAh3: TLabel;
    lblAh2: TLabel;
    lblAh20: TLabel;
    lblAh22: TLabel;
    lblAh21: TLabel;
    lblAh19: TLabel;
    lblAh18: TLabel;
    lblAh17: TLabel;
    lblAh16: TLabel;
    lblAh15: TLabel;
    lblAh14: TLabel;
    lblAh12: TLabel;
    lblAh11: TLabel;
    lblAh10: TLabel;
    lblAh23: TLabel;
    scb2: TScrollBox;
    imgsmKo: TImage;
    imgKo1: TImage;
    imgKo2: TImage;
    imgKo3: TImage;
    imgKo4: TImage;
    imgKo5: TImage;
    imgKo6: TImage;
    imgKo7: TImage;
    imgKo8: TImage;
    imgKo10: TImage;
    imgKo11: TImage;
    imgKo12: TImage;
    imgKo13: TImage;
    imgKo9: TImage;
    lblKo1: TLabel;
    lblKo13: TLabel;
    lblKo7: TLabel;
    lblKo6: TLabel;
    lblKo5: TLabel;
    lblKo4: TLabel;
    lblKo3: TLabel;
    lblKo12: TLabel;
    lblKo11: TLabel;
    lblKo10: TLabel;
    lblKo9: TLabel;
    lblKo2: TLabel;
    lblKo8: TLabel;
    scb3: TScrollBox;
    imgsmAg: TImage;
    imgAg1: TImage;
    imgAg2: TImage;
    imgAg3: TImage;
    imgAg4: TImage;
    imgAg5: TImage;
    imgAg6: TImage;
    lblAg6: TLabel;
    lblAg5: TLabel;
    lblAg4: TLabel;
    lblAg3: TLabel;
    lblAg1: TLabel;
    lblAg2: TLabel;
    lblAh9: TLabel;
    ActionList1: TActionList;
    actPlayPause: TAction;
    actStop: TAction;
    actRewind: TAction;
    actForward: TAction;
    actPrevious: TAction;
    actNext: TAction;
    actVolHigh: TAction;
    actVolLow: TAction;
    actMute: TAction;
    scb4: TScrollBox;
    imgsmFk: TImage;
    imgFk1: TImage;
    imgFk2: TImage;
    imgFk3: TImage;
    lblFk3: TLabel;
    lblFk1: TLabel;
    lblFk2: TLabel;
    scb5: TScrollBox;
    imgsmAv: TImage;
    imgAv1: TImage;
    imgAv2: TImage;
    imgAv3: TImage;
    imgAv4: TImage;
    imgAv5: TImage;
    imgAv6: TImage;
    imgAv7: TImage;
    imgAv8: TImage;
    imgAv10: TImage;
    imgAv11: TImage;
    imgAv12: TImage;
    imgAv13: TImage;
    imgAv14: TImage;
    imgAv15: TImage;
    imgAv16: TImage;
    imgAv17: TImage;
    imgAv18: TImage;
    imgAv19: TImage;
    imgAv20: TImage;
    imgAv21: TImage;
    imgAv22: TImage;
    imgAv23: TImage;
    imgAv9: TImage;
    lblAv8: TLabel;
    lblAv7: TLabel;
    lblAv6: TLabel;
    lblAv5: TLabel;
    lblAv4: TLabel;
    lblAv3: TLabel;
    lblAv23: TLabel;
    lblAv22: TLabel;
    lblAv21: TLabel;
    lblAv20: TLabel;
    lblAv2: TLabel;
    lblAv19: TLabel;
    lblAv18: TLabel;
    lblAv17: TLabel;
    lblAv16: TLabel;
    lblAv15: TLabel;
    lblAv14: TLabel;
    lblAv13: TLabel;
    lblAv12: TLabel;
    lblAv11: TLabel;
    lblAv10: TLabel;
    lblAv1: TLabel;
    lblAv9: TLabel;
    imgAv24: TImage;
    imgAv26: TImage;
    imgAv27: TImage;
    imgAv28: TImage;
    imgAv29: TImage;
    imgAv30: TImage;
    imgAv33: TImage;
    imgAv31: TImage;
    imgAv32: TImage;
    imgAv25: TImage;
    lblAv27: TLabel;
    lblAv28: TLabel;
    lblAv29: TLabel;
    lblAv30: TLabel;
    lblAv33: TLabel;
    lblAv31: TLabel;
    lblAv32: TLabel;
    lblAv24: TLabel;
    lblAv25: TLabel;
    lblAv26: TLabel;
    scb6: TScrollBox;
    imgsmSo: TImage;
    imgSo1: TImage;
    imgSo2: TImage;
    imgSo3: TImage;
    imgSo4: TImage;
    imgSo5: TImage;
    imgSo6: TImage;
    imgSo7: TImage;
    imgSo8: TImage;
    imgSo9: TImage;
    lblSo9: TLabel;
    lblSo7: TLabel;
    lblSo6: TLabel;
    lblSo5: TLabel;
    lblSo4: TLabel;
    lblSo3: TLabel;
    lblSo1: TLabel;
    lblSo2: TLabel;
    lblSo8: TLabel;
    scb7: TScrollBox;
    imgsmMu: TImage;
    imgMu1: TImage;
    imgMu2: TImage;
    imgMu3: TImage;
    imgMu4: TImage;
    imgMu5: TImage;
    imgMu6: TImage;
    imgMu7: TImage;
    imgMu8: TImage;
    imgMu9: TImage;
    lblMu9: TLabel;
    lblMu7: TLabel;
    lblMu6: TLabel;
    lblMu5: TLabel;
    lblMu4: TLabel;
    lblMu3: TLabel;
    lblMu1: TLabel;
    lblMu2: TLabel;
    lblMu8: TLabel;
    scb8: TScrollBox;
    imgsmNa: TImage;
    imgNa1: TImage;
    imgNa2: TImage;
    imgNa3: TImage;
    imgNa4: TImage;
    imgNa5: TImage;
    imgNa6: TImage;
    imgNa7: TImage;
    imgNa8: TImage;
    imgNa10: TImage;
    imgNa11: TImage;
    imgNa12: TImage;
    imgNa13: TImage;
    imgNa9: TImage;
    lblNa9: TLabel;
    lblNa7: TLabel;
    lblNa6: TLabel;
    lblNa5: TLabel;
    lblNa4: TLabel;
    lblNa3: TLabel;
    lblNa13: TLabel;
    lblNa12: TLabel;
    lblNa11: TLabel;
    lblNa10: TLabel;
    lblNa1: TLabel;
    lblNa2: TLabel;
    lblNa8: TLabel;
    lblNa14: TLabel;
    imgNa14: TImage;
    imgPlayerMTitle: TImage;
    imgPlayerSTitle: TImage;
    pnlSound: TPanel;
    lblSound: TLabel;
    lblChange: TLabel;
    procedure lblAg1MouseEnter(Sender: TObject);
    procedure lblAg1MouseLeave(Sender: TObject);
    procedure lblAg2MouseEnter(Sender: TObject);
    procedure lblAg2MouseLeave(Sender: TObject);
    procedure lblAg3MouseEnter(Sender: TObject);
    procedure lblAg3MouseLeave(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure MediaPlayer1Notify(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormShow(Sender: TObject);
    procedure lblLoopClick(Sender: TObject);
    procedure lblRepeatClick(Sender: TObject);
    procedure spbMuteClick(Sender: TObject);
    procedure lblAhClick(Sender: TObject);
    procedure lblFkClick(Sender: TObject);
    procedure lblStopClick(Sender: TObject);
    procedure lblPlayPauseClick(Sender: TObject);
    procedure lblPlayPauseMouseEnter(Sender: TObject);
    procedure lblPlayPauseMouseLeave(Sender: TObject);
    procedure lblStopMouseEnter(Sender: TObject);
    procedure lblStopMouseLeave(Sender: TObject);
    procedure lblRepeatMouseEnter(Sender: TObject);
    procedure lblRepeatMouseLeave(Sender: TObject);
    procedure lblLoopMouseEnter(Sender: TObject);
    procedure lblLoopMouseLeave(Sender: TObject);
    procedure lblTrackUserMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblAg4MouseEnter(Sender: TObject);
    procedure lblAg5MouseEnter(Sender: TObject);
    procedure lblAg6MouseEnter(Sender: TObject);
    procedure lblAg4MouseLeave(Sender: TObject);
    procedure lblAg5MouseLeave(Sender: TObject);
    procedure lblAg6MouseLeave(Sender: TObject);
    procedure lblAhMouseEnter(Sender: TObject);
    procedure lblAhMouseLeave(Sender: TObject);
    procedure PlayFile(FileNo: Byte);
    procedure Imaging(No: Byte);
    procedure Paneling(N: Byte);
    procedure lblFkMouseEnter(Sender: TObject);
    procedure lblFkMouseLeave(Sender: TObject);
    procedure lblForwardClick(Sender: TObject);
    procedure lblRewindClick(Sender: TObject);
    procedure lblNextClick(Sender: TObject);
    procedure lblPreviousClick(Sender: TObject);
    procedure actVolHighExecute(Sender: TObject);
    procedure actVolLowExecute(Sender: TObject);
    procedure imgMainMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblPreviousMouseEnter(Sender: TObject);
    procedure lblPreviousMouseLeave(Sender: TObject);
    procedure lblRewindMouseEnter(Sender: TObject);
    procedure lblRewindMouseLeave(Sender: TObject);
    procedure lblForwardMouseEnter(Sender: TObject);
    procedure lblForwardMouseLeave(Sender: TObject);
    procedure lblNextMouseEnter(Sender: TObject);
    procedure lblNextMouseLeave(Sender: TObject);
    procedure lblKoMouseEnter(Sender: TObject);
    procedure lblKoMouseLeave(Sender: TObject);
    procedure lblAgMouseEnter(Sender: TObject);
    procedure lblAgMouseLeave(Sender: TObject);
    procedure lblAgClick(Sender: TObject);
    procedure lblAvMouseEnter(Sender: TObject);
    procedure lblAvMouseLeave(Sender: TObject);
    procedure lblSoMouseEnter(Sender: TObject);
    procedure lblSoMouseLeave(Sender: TObject);
    procedure lblMuMouseEnter(Sender: TObject);
    procedure lblMuMouseLeave(Sender: TObject);
    procedure lblNaMouseEnter(Sender: TObject);
    procedure lblNaMouseLeave(Sender: TObject);
    procedure lblAboutMouseEnter(Sender: TObject);
    procedure lblAboutMouseLeave(Sender: TObject);
    procedure lblMinMouseEnter(Sender: TObject);
    procedure lblMinMouseLeave(Sender: TObject);
    procedure lblMinClick(Sender: TObject);
    procedure lblExitMouseEnter(Sender: TObject);
    procedure lblExitMouseLeave(Sender: TObject);
    procedure lblExitClick(Sender: TObject);
    procedure lblKoClick(Sender: TObject);
    procedure lblAvClick(Sender: TObject);
    procedure lblSoClick(Sender: TObject);
    procedure lblMuClick(Sender: TObject);
    procedure lblNaClick(Sender: TObject);
    procedure lblAboutClick(Sender: TObject);
    procedure lblClick1(Sender: TObject);
    procedure lblClick10(Sender: TObject);
    procedure lblClick11(Sender: TObject);
    procedure lblClick12(Sender: TObject);
    procedure lblClick13(Sender: TObject);
    procedure lblClick14(Sender: TObject);
    procedure lblClick15(Sender: TObject);
    procedure lblClick16(Sender: TObject);
    procedure lblClick17(Sender: TObject);
    procedure lblClick18(Sender: TObject);
    procedure lblClick19(Sender: TObject);
    procedure lblClick2(Sender: TObject);
    procedure lblClick20(Sender: TObject);
    procedure lblClick21(Sender: TObject);
    procedure lblClick22(Sender: TObject);
    procedure lblClick23(Sender: TObject);
    procedure lblClick24(Sender: TObject);
    procedure lblClick25(Sender: TObject);
    procedure lblClick26(Sender: TObject);
    procedure lblClick27(Sender: TObject);
    procedure lblClick28(Sender: TObject);
    procedure lblClick29(Sender: TObject);
    procedure lblClick3(Sender: TObject);
    procedure lblClick30(Sender: TObject);
    procedure lblClick31(Sender: TObject);
    procedure lblClick32(Sender: TObject);
    procedure lblClick33(Sender: TObject);
    procedure lblClick4(Sender: TObject);
    procedure lblClick5(Sender: TObject);
    procedure lblClick6(Sender: TObject);
    procedure lblClick7(Sender: TObject);
    procedure lblClick8(Sender: TObject);
    procedure lblClick9(Sender: TObject);
    procedure lblAh1MouseEnter(Sender: TObject);
    procedure lblAh1MouseLeave(Sender: TObject);
    procedure lblAh10MouseEnter(Sender: TObject);
    procedure lblAh10MouseLeave(Sender: TObject);
    procedure lblAh11MouseEnter(Sender: TObject);
    procedure lblAh11MouseLeave(Sender: TObject);
    procedure lblAh12MouseEnter(Sender: TObject);
    procedure lblAh12MouseLeave(Sender: TObject);
    procedure lblAh13MouseEnter(Sender: TObject);
    procedure lblAh13MouseLeave(Sender: TObject);
    procedure lblAh14MouseEnter(Sender: TObject);
    procedure lblAh14MouseLeave(Sender: TObject);
    procedure lblAh15MouseEnter(Sender: TObject);
    procedure lblAh15MouseLeave(Sender: TObject);
    procedure lblAh16MouseEnter(Sender: TObject);
    procedure lblAh16MouseLeave(Sender: TObject);
    procedure lblAh17MouseEnter(Sender: TObject);
    procedure lblAh17MouseLeave(Sender: TObject);
    procedure lblAh18MouseEnter(Sender: TObject);
    procedure lblAh18MouseLeave(Sender: TObject);
    procedure lblAh19MouseEnter(Sender: TObject);
    procedure lblAh19MouseLeave(Sender: TObject);
    procedure lblAh20MouseEnter(Sender: TObject);
    procedure lblAh20MouseLeave(Sender: TObject);
    procedure lblAh21MouseEnter(Sender: TObject);
    procedure lblAh21MouseLeave(Sender: TObject);
    procedure lblAh22MouseEnter(Sender: TObject);
    procedure lblAh22MouseLeave(Sender: TObject);
    procedure lblAh23MouseEnter(Sender: TObject);
    procedure lblAh23MouseLeave(Sender: TObject);
    procedure lblAh2MouseEnter(Sender: TObject);
    procedure lblAh2MouseLeave(Sender: TObject);
    procedure lblAh3MouseEnter(Sender: TObject);
    procedure lblAh3MouseLeave(Sender: TObject);
    procedure lblAh4MouseEnter(Sender: TObject);
    procedure lblAh4MouseLeave(Sender: TObject);
    procedure lblAh5MouseEnter(Sender: TObject);
    procedure lblAh5MouseLeave(Sender: TObject);
    procedure lblAh6MouseEnter(Sender: TObject);
    procedure lblAh6MouseLeave(Sender: TObject);
    procedure lblAh7MouseEnter(Sender: TObject);
    procedure lblAh7MouseLeave(Sender: TObject);
    procedure lblAh8MouseEnter(Sender: TObject);
    procedure lblAh8MouseLeave(Sender: TObject);
    procedure lblAh9MouseEnter(Sender: TObject);
    procedure lblAh9MouseLeave(Sender: TObject);
    procedure imgMainClick(Sender: TObject);
    procedure lblKo1MouseEnter(Sender: TObject);
    procedure lblKo1MouseLeave(Sender: TObject);
    procedure lblKo10MouseEnter(Sender: TObject);
    procedure lblKo10MouseLeave(Sender: TObject);
    procedure lblKo11MouseEnter(Sender: TObject);
    procedure lblKo11MouseLeave(Sender: TObject);
    procedure lblKo12MouseEnter(Sender: TObject);
    procedure lblKo12MouseLeave(Sender: TObject);
    procedure lblKo13MouseEnter(Sender: TObject);
    procedure lblKo13MouseLeave(Sender: TObject);
    procedure lblKo2MouseEnter(Sender: TObject);
    procedure lblKo2MouseLeave(Sender: TObject);
    procedure lblKo3MouseEnter(Sender: TObject);
    procedure lblKo3MouseLeave(Sender: TObject);
    procedure lblKo4MouseEnter(Sender: TObject);
    procedure lblKo4MouseLeave(Sender: TObject);
    procedure lblKo5MouseEnter(Sender: TObject);
    procedure lblKo5MouseLeave(Sender: TObject);
    procedure lblKo6MouseEnter(Sender: TObject);
    procedure lblKo6MouseLeave(Sender: TObject);
    procedure lblKo7MouseEnter(Sender: TObject);
    procedure lblKo7MouseLeave(Sender: TObject);
    procedure lblKo8MouseEnter(Sender: TObject);
    procedure lblKo8MouseLeave(Sender: TObject);
    procedure lblKo9MouseEnter(Sender: TObject);
    procedure lblKo9MouseLeave(Sender: TObject);
    procedure lblFk1MouseEnter(Sender: TObject);
    procedure lblFk1MouseLeave(Sender: TObject);
    procedure lblFk2MouseEnter(Sender: TObject);
    procedure lblFk2MouseLeave(Sender: TObject);
    procedure lblFk3MouseEnter(Sender: TObject);
    procedure lblFk3MouseLeave(Sender: TObject);
    procedure lblAv1MouseEnter(Sender: TObject);
    procedure lblAv1MouseLeave(Sender: TObject);
    procedure lblAv10MouseEnter(Sender: TObject);
    procedure lblAv10MouseLeave(Sender: TObject);
    procedure lblAv11MouseEnter(Sender: TObject);
    procedure lblAv11MouseLeave(Sender: TObject);
    procedure lblAv12MouseEnter(Sender: TObject);
    procedure lblAv12MouseLeave(Sender: TObject);
    procedure lblAv13MouseEnter(Sender: TObject);
    procedure lblAv13MouseLeave(Sender: TObject);
    procedure lblAv14MouseEnter(Sender: TObject);
    procedure lblAv14MouseLeave(Sender: TObject);
    procedure lblAv15MouseEnter(Sender: TObject);
    procedure lblAv15MouseLeave(Sender: TObject);
    procedure lblAv16MouseEnter(Sender: TObject);
    procedure lblAv16MouseLeave(Sender: TObject);
    procedure lblAv17MouseEnter(Sender: TObject);
    procedure lblAv17MouseLeave(Sender: TObject);
    procedure lblAv18MouseEnter(Sender: TObject);
    procedure lblAv18MouseLeave(Sender: TObject);
    procedure lblAv19MouseEnter(Sender: TObject);
    procedure lblAv19MouseLeave(Sender: TObject);
    procedure lblAv20MouseEnter(Sender: TObject);
    procedure lblAv20MouseLeave(Sender: TObject);
    procedure lblAv21MouseEnter(Sender: TObject);
    procedure lblAv21MouseLeave(Sender: TObject);
    procedure lblAv22MouseEnter(Sender: TObject);
    procedure lblAv22MouseLeave(Sender: TObject);
    procedure lblAv23MouseEnter(Sender: TObject);
    procedure lblAv23MouseLeave(Sender: TObject);
    procedure lblAv24MouseEnter(Sender: TObject);
    procedure lblAv24MouseLeave(Sender: TObject);
    procedure lblAv25MouseEnter(Sender: TObject);
    procedure lblAv25MouseLeave(Sender: TObject);
    procedure lblAv26MouseEnter(Sender: TObject);
    procedure lblAv26MouseLeave(Sender: TObject);
    procedure lblAv27MouseEnter(Sender: TObject);
    procedure lblAv27MouseLeave(Sender: TObject);
    procedure lblAv28MouseEnter(Sender: TObject);
    procedure lblAv28MouseLeave(Sender: TObject);
    procedure lblAv29MouseEnter(Sender: TObject);
    procedure lblAv29MouseLeave(Sender: TObject);
    procedure lblAv2MouseEnter(Sender: TObject);
    procedure lblAv2MouseLeave(Sender: TObject);
    procedure lblAv30MouseEnter(Sender: TObject);
    procedure lblAv30MouseLeave(Sender: TObject);
    procedure lblAv31MouseEnter(Sender: TObject);
    procedure lblAv31MouseLeave(Sender: TObject);
    procedure lblAv32MouseEnter(Sender: TObject);
    procedure lblAv32MouseLeave(Sender: TObject);
    procedure lblAv33MouseEnter(Sender: TObject);
    procedure lblAv33MouseLeave(Sender: TObject);
    procedure lblAv3MouseEnter(Sender: TObject);
    procedure lblAv3MouseLeave(Sender: TObject);
    procedure lblAv4MouseEnter(Sender: TObject);
    procedure lblAv4MouseLeave(Sender: TObject);
    procedure lblAv5MouseEnter(Sender: TObject);
    procedure lblAv5MouseLeave(Sender: TObject);
    procedure lblAv6MouseEnter(Sender: TObject);
    procedure lblAv6MouseLeave(Sender: TObject);
    procedure lblAv7MouseEnter(Sender: TObject);
    procedure lblAv7MouseLeave(Sender: TObject);
    procedure lblAv8MouseEnter(Sender: TObject);
    procedure lblAv8MouseLeave(Sender: TObject);
    procedure lblAv9MouseEnter(Sender: TObject);
    procedure lblAv9MouseLeave(Sender: TObject);
    procedure lblSo1MouseEnter(Sender: TObject);
    procedure lblSo1MouseLeave(Sender: TObject);
    procedure lblSo2MouseEnter(Sender: TObject);
    procedure lblSo2MouseLeave(Sender: TObject);
    procedure lblSo3MouseEnter(Sender: TObject);
    procedure lblSo3MouseLeave(Sender: TObject);
    procedure lblSo4MouseEnter(Sender: TObject);
    procedure lblSo4MouseLeave(Sender: TObject);
    procedure lblSo5MouseEnter(Sender: TObject);
    procedure lblSo5MouseLeave(Sender: TObject);
    procedure lblSo6MouseEnter(Sender: TObject);
    procedure lblSo6MouseLeave(Sender: TObject);
    procedure lblSo7MouseEnter(Sender: TObject);
    procedure lblSo7MouseLeave(Sender: TObject);
    procedure lblSo8MouseEnter(Sender: TObject);
    procedure lblSo8MouseLeave(Sender: TObject);
    procedure lblSo9MouseEnter(Sender: TObject);
    procedure lblSo9MouseLeave(Sender: TObject);
    procedure lblMu1MouseEnter(Sender: TObject);
    procedure lblMu1MouseLeave(Sender: TObject);
    procedure lblMu2MouseEnter(Sender: TObject);
    procedure lblMu2MouseLeave(Sender: TObject);
    procedure lblMu3MouseEnter(Sender: TObject);
    procedure lblMu3MouseLeave(Sender: TObject);
    procedure lblMu4MouseEnter(Sender: TObject);
    procedure lblMu4MouseLeave(Sender: TObject);
    procedure lblMu5MouseEnter(Sender: TObject);
    procedure lblMu5MouseLeave(Sender: TObject);
    procedure lblMu6MouseEnter(Sender: TObject);
    procedure lblMu6MouseLeave(Sender: TObject);
    procedure lblMu7MouseEnter(Sender: TObject);
    procedure lblMu7MouseLeave(Sender: TObject);
    procedure lblMu8MouseEnter(Sender: TObject);
    procedure lblMu8MouseLeave(Sender: TObject);
    procedure lblMu9MouseEnter(Sender: TObject);
    procedure lblMu9MouseLeave(Sender: TObject);
    procedure lblNa1MouseEnter(Sender: TObject);
    procedure lblNa1MouseLeave(Sender: TObject);
    procedure lblNa10MouseEnter(Sender: TObject);
    procedure lblNa10MouseLeave(Sender: TObject);
    procedure lblNa11MouseEnter(Sender: TObject);
    procedure lblNa11MouseLeave(Sender: TObject);
    procedure lblNa12MouseEnter(Sender: TObject);
    procedure lblNa12MouseLeave(Sender: TObject);
    procedure lblNa13MouseEnter(Sender: TObject);
    procedure lblNa13MouseLeave(Sender: TObject);
    procedure lblNa14MouseEnter(Sender: TObject);
    procedure lblNa14MouseLeave(Sender: TObject);
    procedure lblNa2MouseEnter(Sender: TObject);
    procedure lblNa2MouseLeave(Sender: TObject);
    procedure lblNa3MouseEnter(Sender: TObject);
    procedure lblNa3MouseLeave(Sender: TObject);
    procedure lblNa4MouseEnter(Sender: TObject);
    procedure lblNa4MouseLeave(Sender: TObject);
    procedure lblNa5MouseEnter(Sender: TObject);
    procedure lblNa5MouseLeave(Sender: TObject);
    procedure lblNa6MouseEnter(Sender: TObject);
    procedure lblNa6MouseLeave(Sender: TObject);
    procedure lblNa7MouseEnter(Sender: TObject);
    procedure lblNa7MouseLeave(Sender: TObject);
    procedure lblNa8MouseEnter(Sender: TObject);
    procedure lblNa8MouseLeave(Sender: TObject);
    procedure lblNa9MouseEnter(Sender: TObject);
    procedure lblNa9MouseLeave(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure lblChangeMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    procedure PanelsData;
    function MiliSecondToTime(Sec: Longint): AnsiString;
    procedure UpdateCurrTime;
    function UpdateFileName(n: Byte): String;
    procedure CurPanelNumbering(n: Byte);
    procedure CurSToCur;
    procedure LabelsClick(n: Byte);
    procedure SoundHeightShow;
    procedure ChangeSound(v: Smallint);
//    function GetWinDir: TFileName;
    { Private declarations }
  public
    { Public declarations }
  end;

procedure SetMute;
procedure ResetMute;

const
  MaxPanelNo = 8;

type
  PanelData = record
    Name: String;
    MaxNo: Byte;
  end;

var
  frmMainNeiNava: TfrmMainNeiNava;
  arrPanels: array [1..MaxPanelNo] of PanelData;
  NowPlay: Byte;  // 0=none
  Vol: Byte;
  Mute: Boolean;
  Rep, Loop: Boolean;
  CurPanel, CurPanelS: Byte;

implementation

uses Math, untAbout, untExit;

{$R *.dfm}

procedure TfrmMainNeiNava.lblAg1MouseEnter(Sender: TObject);
begin
  imgAg1.Visible := True;
end;

procedure TfrmMainNeiNava.lblAg1MouseLeave(Sender: TObject);
begin
  imgAg1.Visible := False;
end;

procedure TfrmMainNeiNava.lblAg2MouseEnter(Sender: TObject);
begin
  imgAg2.Visible := True;
end;

procedure TfrmMainNeiNava.lblAg2MouseLeave(Sender: TObject);
begin
  imgAg2.Visible := False;
end;

procedure TfrmMainNeiNava.lblAg3MouseEnter(Sender: TObject);
begin
  imgAg3.Visible := True;
end;

procedure TfrmMainNeiNava.lblAg3MouseLeave(Sender: TObject);
begin
  imgAg3.Visible := False;
end;

procedure TfrmMainNeiNava.PanelsData;
begin
  arrPanels[1].Name := 'Ah'; // Ahangaran
  arrPanels[1].MaxNo := 23;
  arrPanels[2].Name := 'Ko'; // Koveiti Poor
  arrPanels[2].MaxNo := 13;
  arrPanels[3].Name := 'Ag'; //Aghasi
  arrPanels[3].MaxNo := 6;
  arrPanels[4].Name := 'Fk'; //Fakhri
  arrPanels[4].MaxNo := 3;
  arrPanels[5].Name := 'Av'; //Avini
  arrPanels[5].MaxNo := 33;
  arrPanels[6].Name := 'So'; // Sorood
  arrPanels[6].MaxNo := 9;
  arrPanels[7].Name := 'Mu'; // Music
  arrPanels[7].MaxNo := 9;
  arrPanels[8].Name := 'Na'; // Navai-e Del
  arrPanels[8].MaxNo := 14;
end;

procedure TfrmMainNeiNava.FormCreate(Sender: TObject);
begin
  PanelsData;
  Rep := False;
  Loop := False;
  NowPlay := 0; // No file is playing.
  Vol := mVolControl1.Volume;
  Mute := False;
  CurPanel := 0;
  CurPanelS := 0;
  if Mute then
    spbMute.Down := True
  else
    spbMute.Down := False;
  SoundHeightShow;
end;

procedure SetMute;
begin
  Mute := True;
end;

procedure ResetMute;
begin
  Mute := False;
end;

(*function TForm1.GetWinDir: TFileName;
var
  SysFolder: String;
begin
  SetLength(SysFolder, 255);
  SetLength(SysFolder, GetWindowsDirectory(
    PChar(SysFolder), Length(SysFolder)));
  Result := SysFolder;
end;*)

procedure TfrmMainNeiNava.UpdateCurrTime;
var
  cp, l, t: Longint; // Current Position, Length
begin
  try
    cp := MediaPlayer1.Position;// div 1000;
    l := MediaPlayer1.Length;// div 1000;
    t := cp * lblTrackUser.Width div l;
    if not (t >= 344) then  // 345 is the length of lblTrackbar
      t := (t div 5) * 5;
    imgTrackMove.Width := t;
//    imgTrackMove.Width := lblTrackUser.Width - t;
//    imgTrackMove.Left := lblTrackUser.Left + t;
    lblTime.Caption := MiliSecondToTime(cp);
  except
    ;
  end;
end;

procedure TfrmMainNeiNava.Timer1Timer(Sender: TObject);
begin
  try
    UpdateCurrTime;
    if MediaPlayer1.Position
      >= MediaPlayer1.Length then
    begin
      if Rep then
      begin
        MediaPlayer1.Rewind;
        MediaPlayer1.Play;
      end
      else
      begin
        if Loop then
        begin
     //     if NowPlay in [1..MaxFileNo] then
     //     begin
            if NowPlay + 1 > arrPanels[CurPanel].MaxNo then
            begin
              CurPanel := CurPanel + 1;
              CurPanelNumbering(CurPanel);
              PlayFile(1)
            end
            else
              PlayFile(NowPlay+1);
      //    end;
        end
        else
          lblStopClick(Sender);
      end;
    end;
  except
    ;
  end;
end;

procedure TfrmMainNeiNava.MediaPlayer1Notify(Sender: TObject);
begin
  with Sender as TMediaPlayer do
  begin
    Notify := True;
    if (Mode = mpPaused) or (Mode = mpStopped) then
    begin
      imgPause.Visible := False;
      imgPauseO.Visible := False;
    end
    else if Mode = mpPlaying then
    begin
      imgPlay.Visible := False;
      imgPause.Visible := True;
    end;
  end;
end;

procedure TfrmMainNeiNava.FormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
const
  SC_DragMove = $F012;
begin
  ReleaseCapture;
  Perform(WM_SYSCOMMAND, SC_DragMove, 0);
end;

procedure TfrmMainNeiNava.FormShow(Sender: TObject);
begin
  MediaPlayer1.Notify := True;
end;

function TfrmMainNeiNava.UpdateFileName(n: Byte): String;
var
  fn, z: String;
begin
  if n < 10 then
    z := '0'
  else
    z := '';
  fn := '.\' + IntToStr(CurPanel) + arrPanels[CurPanel].Name
    + z + IntToStr(n) + '.mp3';
  if fn <> MediaPlayer1.FileName then
  begin
    imgPlayerMTitle.Picture.LoadFromFile('.\Data\' +
      arrPanels[CurPanel].Name + '.jpg');
    imgPlayerSTitle.Picture.LoadFromFile('.\Data\' +
      arrPanels[CurPanel].Name + IntToStr(n) + '.jpg');
    try
      MediaPlayer1.Close;
    except
      ;
    end;
    imgTrackMove.Width := 0;
    MediaPlayer1.FileName := fn;
    MediaPlayer1.Open;
    lblPlayPause.Enabled := True;
    lblFullTime.Caption := MiliSecondToTime(MediaPlayer1.Length);
    NowPlay := n;
    Imaging(NowPlay);
  end;
end;

procedure TfrmMainNeiNava.PlayFile(FileNo: Byte);
begin
  UpdateFileName(FileNo);
  MediaPlayer1.Play;
  MediaPlayer1Notify(MediaPlayer1);
  Timer1.Enabled := True;
end;

function TfrmMainNeiNava.MiliSecondToTime(Sec: Longint): AnsiString;
var
  h, m, s: Byte;
  TimeStr: AnsiString;
begin
  Sec := Sec div 1000;
  m := Sec div 60;
  s := Sec - (m * 60);
  h := 0;
  if m >= 60 then
  begin
    h := m div 60;
    m := m - (h * 60);
  end;
  TimeStr := '';
  if h <> 0 then
    if m < 10 then
      TimeStr := IntToStr(h) + ':0'
    else
      TimeStr := IntToStr(h) + ':';
  TimeStr := TimeStr + IntToStr(m) + ':';
  if s < 10 then
    TimeStr := TimeStr + '0';
  TimeStr := TimeStr + IntToStr(s);
  Result := TimeStr;
end;

procedure TfrmMainNeiNava.Imaging(No: Byte);
begin
(*  imgS01.Visible := False;
  imgS02.Visible := False;
  imgS03.Visible := False;
  imgS04.Visible := False;
  imgS05.Visible := False;
  imgS06.Visible := False;
  imgS07.Visible := False;
  imgS08.Visible := False;
  imgS09.Visible := False;
  imgS10.Visible := False;
  imgS11.Visible := False;
  imgS12.Visible := False;
  imgS13.Visible := False;
  imgS14.Visible := False;
  case No of
    11: imgS01.Visible := True;
    12: imgS02.Visible := True;
    13: imgS03.Visible := True;
    14: imgS04.Visible := True;
    15: imgS05.Visible := True;
    16: imgS06.Visible := True;
    17: imgS07.Visible := True;
    18: imgS08.Visible := True;
    19: imgS09.Visible := True;
    20: imgS10.Visible := True;
    21: imgS11.Visible := True;
    22: imgS12.Visible := True;
    23: imgS13.Visible := True;
    24: imgS14.Visible := True;
  end; *)
end;

procedure TfrmMainNeiNava.lblLoopClick(Sender: TObject);
begin
  Loop := not Loop;
  if Loop then
  begin
    Rep := False;
    imgRepeat.Visible := False;
  end;
end;

procedure TfrmMainNeiNava.lblRepeatClick(Sender: TObject);
begin
  Rep := not Rep;
  if Rep then
  begin
    Loop := False;
    imgLoop.Visible := False;
  end;
end;

procedure TfrmMainNeiNava.spbMuteClick(Sender: TObject);
begin
//  if spbMute.Down then
  if not Mute then
  begin
    mVolControl1.Volume := 0;
    spbMute.Down := True;
    SetMute;
  end
  else
  begin
    mVolControl1.Volume := Vol;
    SoundHeightShow;
    spbMute.Down := False;
    ResetMute;
  end;
end;

procedure TfrmMainNeiNava.SoundHeightShow;
begin
  lblSound.Height := lblChange.Height -
    ((Vol * lblChange.Height) div 255);
end;

procedure TfrmMainNeiNava.CurPanelNumbering(n: Byte);
begin
  if N < 1 then
    CurPanel := MaxPanelNo
  else if N > MaxPanelNo then
    CurPanel := 1
  else
    CurPanel := N;
end;

procedure TfrmMainNeiNava.CurSToCur;
begin
  CurPanel := CurPanelS;
end;

procedure TfrmMainNeiNava.Paneling(N: Byte);
begin
  scb1.Visible := False;
  scb2.Visible := False;
  scb3.Visible := False;
  scb4.Visible := False;
  scb5.Visible := False;
  scb6.Visible := False;
  scb7.Visible := False;
  scb8.Visible := False;
  imgsbAh.Visible := False;
  imgsbKo.Visible := False;
  imgsbAg.Visible := False;
  imgsbFk.Visible := False;
  imgsbAv.Visible := False;
  imgsbSo.Visible := False;
  imgsbMu.Visible := False;
  imgsbNa.Visible := False;
  if N = 0 then
  begin
    CurPanelS := 0;
    pnlMenuPanel.Visible := False;
    Exit;
  end;
  pnlMenuPanel.Visible := True;
  if N < 1 then
    CurPanelS := MaxPanelNo
  else if N > MaxPanelNo then
    CurPanelS := 1
  else
    CurPanelS := N;
  case N of
    1:begin
        scb1.Visible := True;
        imgsbAh.Visible := True;
      end;
    2:begin
        scb2.Visible := True;
        imgsbKo.Visible := True;
      end;
    3:begin
        scb3.Visible := True;
        imgsbAg.Visible := True;
      end;
    4:begin
        scb4.Visible := True;
        imgsbFk.Visible := True;
      end;
    5:begin
        scb5.Visible := True;
        imgsbAv.Visible := True;
      end;
    6:begin
        scb6.Visible := True;
        imgsbSo.Visible := True;
      end;
    7:begin
        scb7.Visible := True;
        imgsbMu.Visible := True;
      end;
    8:begin
        scb8.Visible := True;
        imgsbNa.Visible := True;
      end;
  end;
end;

procedure TfrmMainNeiNava.lblAhClick(Sender: TObject);
begin
  Paneling(1);
end;

procedure TfrmMainNeiNava.lblFkClick(Sender: TObject);
begin
  Paneling(4);
end;

procedure TfrmMainNeiNava.lblStopClick(Sender: TObject);
begin
  MediaPlayer1.Notify := True;
  try
    imgTrackMove.Width := lblTrackUser.Width;
    imgTrackMove.Left := lblTrackUser.Left;
    Timer1.Enabled := False;
    MediaPlayer1.Rewind;
    MediaPlayer1.Stop;
    imgTrackMove.Width := 0;
  except
    ;
  end;
end;

procedure TfrmMainNeiNava.lblPlayPauseClick(Sender: TObject);
begin
  MediaPlayer1.Notify := True;
  if NowPlay <> 0 then
  begin
    if imgPause.Visible then
      MediaPlayer1.Pause
    else
    begin
      Timer1.Enabled := True;
      MediaPlayer1.Play;
      MediaPlayer1Notify(MediaPlayer1);
    end;
  end;
end;

procedure TfrmMainNeiNava.lblPlayPauseMouseEnter(Sender: TObject);
begin
  if (NowPlay <> 0) then
    if imgPause.Visible then
      imgPauseO.Visible := True
    else if not imgPause.Visible then
      imgPlay.Visible := True;
end;

procedure TfrmMainNeiNava.lblPlayPauseMouseLeave(Sender: TObject);
begin
  if imgPauseO.Visible then
    imgPauseO.Visible := False;
  if imgPlay.Visible then
    imgPlay.Visible := False;
end;

procedure TfrmMainNeiNava.lblStopMouseEnter(Sender: TObject);
begin
  if (NowPlay <> 0) then
    imgStop.Visible := True;
end;

procedure TfrmMainNeiNava.lblStopMouseLeave(Sender: TObject);
begin
  imgStop.Visible := False;
end;

procedure TfrmMainNeiNava.lblRepeatMouseEnter(Sender: TObject);
begin
  imgRepeat.Visible := True;
end;

procedure TfrmMainNeiNava.lblRepeatMouseLeave(Sender: TObject);
begin
  if not Rep then
    imgRepeat.Visible := False;
end;

procedure TfrmMainNeiNava.lblLoopMouseEnter(Sender: TObject);
begin
  imgLoop.Visible := True;
end;

procedure TfrmMainNeiNava.lblLoopMouseLeave(Sender: TObject);
begin
  if not Loop then
    imgLoop.Visible := False;
end;

procedure TfrmMainNeiNava.lblTrackUserMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  l, t: Longint; // Length
begin
  try
    l := MediaPlayer1.Length;// div 1000;
//    t := (lblTrackUser.Width - X) * l
  //    div lblTrackUser.Width;
    t := X * l div lblTrackUser.Width;
    if not Timer1.Enabled then
    begin
//      imgTrackMove.Width := lblTrackUser.Width - X;
//      imgTrackMove.Left := lblTrackUser.Left + X;
      imgTrackMove.Width := X;
    end;
    MediaPlayer1.Position := t;
    MediaPlayer1.Notify := True;
    if imgPause.Visible then
      MediaPlayer1.Play;
  except
    ;
  end;
end;

procedure TfrmMainNeiNava.lblAg4MouseEnter(Sender: TObject);
begin
  imgAg4.Visible := True;
end;

procedure TfrmMainNeiNava.lblAg5MouseEnter(Sender: TObject);
begin
  imgAg5.Visible := True;
end;

procedure TfrmMainNeiNava.lblAg6MouseEnter(Sender: TObject);
begin
  imgAg6.Visible := True;
end;

procedure TfrmMainNeiNava.lblAg4MouseLeave(Sender: TObject);
begin
  imgAg4.Visible := False;
end;

procedure TfrmMainNeiNava.lblAg5MouseLeave(Sender: TObject);
begin
  imgAg5.Visible := False;
end;

procedure TfrmMainNeiNava.lblAg6MouseLeave(Sender: TObject);
begin
  imgAg6.Visible := False;
end;

procedure TfrmMainNeiNava.lblAhMouseEnter(Sender: TObject);
begin
  imgAh.Visible := True;
end;

procedure TfrmMainNeiNava.lblAhMouseLeave(Sender: TObject);
begin
  imgAh.Visible := False;
end;

procedure TfrmMainNeiNava.lblFkMouseEnter(Sender: TObject);
begin
  imgFk.Visible := True;
end;

procedure TfrmMainNeiNava.lblFkMouseLeave(Sender: TObject);
begin
  imgFk.Visible := False;
end;

procedure TfrmMainNeiNava.lblForwardClick(Sender: TObject);
var
  Playing: Boolean;
begin
  if NowPlay <> 0 then
  begin
    if MediaPlayer1.Mode = mpPlaying then
      Playing := True
    else
      Playing := False;
    MediaPlayer1.Position := MediaPlayer1.Position + 10000;
    UpdateCurrTime;
    if Playing then
      MediaPlayer1.Play;
    MediaPlayer1.Notify := True;
  end;
end;

procedure TfrmMainNeiNava.lblRewindClick(Sender: TObject);
var
  Playing: Boolean;
begin
  if NowPlay <> 0 then
  begin
    if MediaPlayer1.Mode = mpPlaying then
      Playing := True
    else
      Playing := False;
    MediaPlayer1.Position := MediaPlayer1.Position - 10000;
    UpdateCurrTime;
    if Playing then
      MediaPlayer1.Play;
    MediaPlayer1.Notify := True;
  end;
end;

procedure TfrmMainNeiNava.lblNextClick(Sender: TObject);
begin
  if NowPlay <> 0 then
  begin
    NowPlay := NowPlay + 1;
    if NowPlay > arrPanels[CurPanel].MaxNo then
    begin
      CurPanel := CurPanel + 1;
      CurPanelNumbering(CurPanel);
      NowPlay := 1;
    end;
    if (MediaPlayer1.Mode in
      [mpPlaying, mpPaused]) then
      PlayFile(NowPlay)
    else
      UpdateFileName(NowPlay);
  end;
  MediaPlayer1.Notify := True;
end;

procedure TfrmMainNeiNava.lblPreviousClick(Sender: TObject);
begin
  if NowPlay <> 0 then
  begin
    NowPlay := NowPlay - 1;
    if NowPlay < 1 then
    begin
      CurPanel := CurPanel - 1;
      CurPanelNumbering(CurPanel);
      NowPlay := arrPanels[CurPanel].MaxNo;
    end;
    if (MediaPlayer1.Mode in
      [mpPlaying, mpPaused]) then
      PlayFile(NowPlay)
    else
      UpdateFileName(NowPlay);
  end;
  MediaPlayer1.Notify := True;
end;

procedure TfrmMainNeiNava.actVolHighExecute(Sender: TObject);
begin
  ChangeSound(Vol + 26);
end;

procedure TfrmMainNeiNava.actVolLowExecute(Sender: TObject);
begin
  ChangeSound(Vol - 26);
end;

procedure TfrmMainNeiNava.imgMainMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
const
  SC_DragMove = $F012;
begin
  Paneling(0);
  ReleaseCapture;
  Perform(WM_SYSCOMMAND, SC_DragMove, 0);
end;

procedure TfrmMainNeiNava.lblPreviousMouseEnter(Sender: TObject);
begin
  imgPre.Visible := True;
end;

procedure TfrmMainNeiNava.lblPreviousMouseLeave(Sender: TObject);
begin
  imgPre.Visible := False;
end;

procedure TfrmMainNeiNava.lblRewindMouseEnter(Sender: TObject);
begin
  imgRewind.Visible := True;
end;

procedure TfrmMainNeiNava.lblRewindMouseLeave(Sender: TObject);
begin
  imgRewind.Visible := False;
end;

procedure TfrmMainNeiNava.lblForwardMouseEnter(Sender: TObject);
begin
  imgForward.Visible := True;
end;

procedure TfrmMainNeiNava.lblForwardMouseLeave(Sender: TObject);
begin
  imgForward.Visible := False;
end;

procedure TfrmMainNeiNava.lblNextMouseEnter(Sender: TObject);
begin
  imgNext.Visible := True;
end;

procedure TfrmMainNeiNava.lblNextMouseLeave(Sender: TObject);
begin
  imgNext.Visible := False;
end;

procedure TfrmMainNeiNava.lblKoMouseEnter(Sender: TObject);
begin
  imgKo.Visible := True;
end;

procedure TfrmMainNeiNava.lblKoMouseLeave(Sender: TObject);
begin
  imgKo.Visible := False;
end;

procedure TfrmMainNeiNava.lblAgMouseEnter(Sender: TObject);
begin
  imgAg.Visible := True;
end;

procedure TfrmMainNeiNava.lblAgMouseLeave(Sender: TObject);
begin
  imgAg.Visible := False;
end;

procedure TfrmMainNeiNava.lblAgClick(Sender: TObject);
begin
  Paneling(3);
end;

procedure TfrmMainNeiNava.lblAvMouseEnter(Sender: TObject);
begin
  imgAv.Visible := True;
end;

procedure TfrmMainNeiNava.lblAvMouseLeave(Sender: TObject);
begin
  imgAv.Visible := False;
end;

procedure TfrmMainNeiNava.lblSoMouseEnter(Sender: TObject);
begin
  imgSo.Visible := True;
end;

procedure TfrmMainNeiNava.lblSoMouseLeave(Sender: TObject);
begin
  imgSo.Visible := False;
end;

procedure TfrmMainNeiNava.lblMuMouseEnter(Sender: TObject);
begin
  imgMu.Visible := True;
end;

procedure TfrmMainNeiNava.lblMuMouseLeave(Sender: TObject);
begin
  imgMu.Visible := False;
end;

procedure TfrmMainNeiNava.lblNaMouseEnter(Sender: TObject);
begin
  imgNa.Visible := True;
end;

procedure TfrmMainNeiNava.lblNaMouseLeave(Sender: TObject);
begin
  imgNa.Visible := False;
end;

procedure TfrmMainNeiNava.lblAboutMouseEnter(Sender: TObject);
begin
  imgAbout.Visible := True;
end;

procedure TfrmMainNeiNava.lblAboutMouseLeave(Sender: TObject);
begin
  imgAbout.Visible := False;
end;

procedure TfrmMainNeiNava.lblMinMouseEnter(Sender: TObject);
begin
  imgMin.Visible := True;
end;

procedure TfrmMainNeiNava.lblMinMouseLeave(Sender: TObject);
begin
  imgMin.Visible := False;
end;

procedure TfrmMainNeiNava.lblMinClick(Sender: TObject);
begin
  Application.Minimize;
end;

procedure TfrmMainNeiNava.lblExitMouseEnter(Sender: TObject);
begin
  imgExit.Visible := True;
end;

procedure TfrmMainNeiNava.lblExitMouseLeave(Sender: TObject);
begin
  imgExit.Visible := False;
end;

procedure TfrmMainNeiNava.lblExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmMainNeiNava.lblKoClick(Sender: TObject);
begin
  Paneling(2);
end;

procedure TfrmMainNeiNava.lblAvClick(Sender: TObject);
begin
  Paneling(5);
end;

procedure TfrmMainNeiNava.lblSoClick(Sender: TObject);
begin
  Paneling(6);
end;

procedure TfrmMainNeiNava.lblMuClick(Sender: TObject);
begin
  Paneling(7);
end;

procedure TfrmMainNeiNava.lblNaClick(Sender: TObject);
begin
  Paneling(8);
end;

procedure TfrmMainNeiNava.lblAboutClick(Sender: TObject);
begin
  frmAbout.ShowModal;
end;

procedure TfrmMainNeiNava.LabelsClick(n: Byte);
begin
  CurSToCur;
  PlayFile(n);
end;

procedure TfrmMainNeiNava.lblClick1(Sender: TObject);
begin
  LabelsClick(1);
end;

procedure TfrmMainNeiNava.lblClick2(Sender: TObject);
begin
  LabelsClick(2);
end;

procedure TfrmMainNeiNava.lblClick3(Sender: TObject);
begin
  LabelsClick(3);
end;

procedure TfrmMainNeiNava.lblClick4(Sender: TObject);
begin
  LabelsClick(4);
end;

procedure TfrmMainNeiNava.lblClick5(Sender: TObject);
begin
  LabelsClick(5);
end;

procedure TfrmMainNeiNava.lblClick6(Sender: TObject);
begin
  LabelsClick(6);
end;

procedure TfrmMainNeiNava.lblClick7(Sender: TObject);
begin
  LabelsClick(7);
end;

procedure TfrmMainNeiNava.lblClick8(Sender: TObject);
begin
  LabelsClick(8);
end;

procedure TfrmMainNeiNava.lblClick9(Sender: TObject);
begin
  LabelsClick(9);
end;

procedure TfrmMainNeiNava.lblClick10(Sender: TObject);
begin
  LabelsClick(10);
end;

procedure TfrmMainNeiNava.lblClick11(Sender: TObject);
begin
  LabelsClick(11);
end;

procedure TfrmMainNeiNava.lblClick12(Sender: TObject);
begin
  LabelsClick(12);
end;

procedure TfrmMainNeiNava.lblClick13(Sender: TObject);
begin
  LabelsClick(13);
end;

procedure TfrmMainNeiNava.lblClick14(Sender: TObject);
begin
  LabelsClick(14);
end;

procedure TfrmMainNeiNava.lblClick15(Sender: TObject);
begin
  LabelsClick(15);
end;

procedure TfrmMainNeiNava.lblClick16(Sender: TObject);
begin
  LabelsClick(16);
end;

procedure TfrmMainNeiNava.lblClick17(Sender: TObject);
begin
  LabelsClick(17);
end;

procedure TfrmMainNeiNava.lblClick18(Sender: TObject);
begin
  LabelsClick(18);
end;

procedure TfrmMainNeiNava.lblClick19(Sender: TObject);
begin
  LabelsClick(19);
end;

procedure TfrmMainNeiNava.lblClick20(Sender: TObject);
begin
  LabelsClick(20);
end;

procedure TfrmMainNeiNava.lblClick21(Sender: TObject);
begin
  LabelsClick(21);
end;

procedure TfrmMainNeiNava.lblClick22(Sender: TObject);
begin
  LabelsClick(22);
end;

procedure TfrmMainNeiNava.lblClick23(Sender: TObject);
begin
  LabelsClick(23);
end;

procedure TfrmMainNeiNava.lblClick24(Sender: TObject);
begin
  LabelsClick(24);
end;

procedure TfrmMainNeiNava.lblClick25(Sender: TObject);
begin
  LabelsClick(25);
end;

procedure TfrmMainNeiNava.lblClick26(Sender: TObject);
begin
  LabelsClick(26);
end;

procedure TfrmMainNeiNava.lblClick27(Sender: TObject);
begin
  LabelsClick(27);
end;

procedure TfrmMainNeiNava.lblClick28(Sender: TObject);
begin
  LabelsClick(28);
end;

procedure TfrmMainNeiNava.lblClick29(Sender: TObject);
begin
  LabelsClick(29);
end;

procedure TfrmMainNeiNava.lblClick30(Sender: TObject);
begin
  LabelsClick(30);
end;

procedure TfrmMainNeiNava.lblClick31(Sender: TObject);
begin
  LabelsClick(31);
end;

procedure TfrmMainNeiNava.lblClick32(Sender: TObject);
begin
  LabelsClick(32);
end;

procedure TfrmMainNeiNava.lblClick33(Sender: TObject);
begin
  LabelsClick(33);
end;

procedure TfrmMainNeiNava.lblAh1MouseEnter(Sender: TObject);
begin
  imgAh1.Visible := True;
end;

procedure TfrmMainNeiNava.lblAh1MouseLeave(Sender: TObject);
begin
  imgAh1.Visible := False;
end;

procedure TfrmMainNeiNava.lblAh2MouseEnter(Sender: TObject);
begin
  imgAh2.Visible := True;
end;

procedure TfrmMainNeiNava.lblAh2MouseLeave(Sender: TObject);
begin
  imgAh2.Visible := False;
end;

procedure TfrmMainNeiNava.lblAh3MouseEnter(Sender: TObject);
begin
  imgAh3.Visible := True;
end;

procedure TfrmMainNeiNava.lblAh3MouseLeave(Sender: TObject);
begin
  imgAh3.Visible := False;
end;

procedure TfrmMainNeiNava.lblAh4MouseEnter(Sender: TObject);
begin
  imgAh4.Visible := True;
end;

procedure TfrmMainNeiNava.lblAh4MouseLeave(Sender: TObject);
begin
  imgAh4.Visible := False;
end;

procedure TfrmMainNeiNava.lblAh5MouseEnter(Sender: TObject);
begin
  imgAh5.Visible := True;
end;

procedure TfrmMainNeiNava.lblAh5MouseLeave(Sender: TObject);
begin
  imgAh5.Visible := False;
end;

procedure TfrmMainNeiNava.lblAh6MouseEnter(Sender: TObject);
begin
  imgAh6.Visible := True;
end;

procedure TfrmMainNeiNava.lblAh6MouseLeave(Sender: TObject);
begin
  imgAh6.Visible := False;
end;

procedure TfrmMainNeiNava.lblAh7MouseEnter(Sender: TObject);
begin
  imgAh7.Visible := True;
end;

procedure TfrmMainNeiNava.lblAh7MouseLeave(Sender: TObject);
begin
  imgAh7.Visible := False;
end;

procedure TfrmMainNeiNava.lblAh8MouseEnter(Sender: TObject);
begin
  imgAh8.Visible := True;
end;

procedure TfrmMainNeiNava.lblAh8MouseLeave(Sender: TObject);
begin
  imgAh8.Visible := False;
end;

procedure TfrmMainNeiNava.lblAh9MouseEnter(Sender: TObject);
begin
  imgAh9.Visible := True;
end;

procedure TfrmMainNeiNava.lblAh9MouseLeave(Sender: TObject);
begin
  imgAh9.Visible := False;
end;

procedure TfrmMainNeiNava.lblAh10MouseEnter(Sender: TObject);
begin
  imgAh10.Visible := True;
end;

procedure TfrmMainNeiNava.lblAh10MouseLeave(Sender: TObject);
begin
  imgAh10.Visible := False;
end;

procedure TfrmMainNeiNava.lblAh11MouseEnter(Sender: TObject);
begin
  imgAh11.Visible := True;
end;

procedure TfrmMainNeiNava.lblAh11MouseLeave(Sender: TObject);
begin
  imgAh11.Visible := False;
end;

procedure TfrmMainNeiNava.lblAh12MouseEnter(Sender: TObject);
begin
  imgAh12.Visible := True;
end;

procedure TfrmMainNeiNava.lblAh12MouseLeave(Sender: TObject);
begin
  imgAh12.Visible := False;
end;

procedure TfrmMainNeiNava.lblAh13MouseEnter(Sender: TObject);
begin
  imgAh13.Visible := True;
end;

procedure TfrmMainNeiNava.lblAh13MouseLeave(Sender: TObject);
begin
  imgAh13.Visible := False;
end;

procedure TfrmMainNeiNava.lblAh14MouseEnter(Sender: TObject);
begin
  imgAh14.Visible := True;
end;

procedure TfrmMainNeiNava.lblAh14MouseLeave(Sender: TObject);
begin
  imgAh14.Visible := False;
end;

procedure TfrmMainNeiNava.lblAh15MouseEnter(Sender: TObject);
begin
  imgAh15.Visible := True;
end;

procedure TfrmMainNeiNava.lblAh15MouseLeave(Sender: TObject);
begin
  imgAh15.Visible := False;
end;

procedure TfrmMainNeiNava.lblAh16MouseEnter(Sender: TObject);
begin
  imgAh16.Visible := True;
end;

procedure TfrmMainNeiNava.lblAh16MouseLeave(Sender: TObject);
begin
  imgAh16.Visible := False;
end;

procedure TfrmMainNeiNava.lblAh17MouseEnter(Sender: TObject);
begin
  imgAh17.Visible := True;
end;

procedure TfrmMainNeiNava.lblAh17MouseLeave(Sender: TObject);
begin
  imgAh17.Visible := False;
end;

procedure TfrmMainNeiNava.lblAh18MouseEnter(Sender: TObject);
begin
  imgAh18.Visible := True;
end;

procedure TfrmMainNeiNava.lblAh18MouseLeave(Sender: TObject);
begin
  imgAh18.Visible := False;
end;

procedure TfrmMainNeiNava.lblAh19MouseEnter(Sender: TObject);
begin
  imgAh19.Visible := True;
end;

procedure TfrmMainNeiNava.lblAh19MouseLeave(Sender: TObject);
begin
  imgAh19.Visible := False;
end;

procedure TfrmMainNeiNava.lblAh20MouseEnter(Sender: TObject);
begin
  imgAh20.Visible := True;
end;

procedure TfrmMainNeiNava.lblAh20MouseLeave(Sender: TObject);
begin
  imgAh20.Visible := False;
end;

procedure TfrmMainNeiNava.lblAh21MouseEnter(Sender: TObject);
begin
  imgAh21.Visible := True;
end;

procedure TfrmMainNeiNava.lblAh21MouseLeave(Sender: TObject);
begin
  imgAh21.Visible := False;
end;

procedure TfrmMainNeiNava.lblAh22MouseEnter(Sender: TObject);
begin
  imgAh22.Visible := True;
end;

procedure TfrmMainNeiNava.lblAh22MouseLeave(Sender: TObject);
begin
  imgAh22.Visible := False;
end;

procedure TfrmMainNeiNava.lblAh23MouseEnter(Sender: TObject);
begin
  imgAh23.Visible := True;
end;

procedure TfrmMainNeiNava.lblAh23MouseLeave(Sender: TObject);
begin
  imgAh23.Visible := False;
end;

procedure TfrmMainNeiNava.imgMainClick(Sender: TObject);
begin
  Paneling(0);
end;

procedure TfrmMainNeiNava.lblKo1MouseEnter(Sender: TObject);
begin
  imgKo1.Visible := True;
end;

procedure TfrmMainNeiNava.lblKo1MouseLeave(Sender: TObject);
begin
  imgKo1.Visible := False;
end;

procedure TfrmMainNeiNava.lblKo2MouseEnter(Sender: TObject);
begin
  imgKo2.Visible := True;
end;

procedure TfrmMainNeiNava.lblKo2MouseLeave(Sender: TObject);
begin
  imgKo2.Visible := False;
end;

procedure TfrmMainNeiNava.lblKo3MouseEnter(Sender: TObject);
begin
  imgKo3.Visible := True;
end;

procedure TfrmMainNeiNava.lblKo3MouseLeave(Sender: TObject);
begin
  imgKo3.Visible := False;
end;

procedure TfrmMainNeiNava.lblKo4MouseEnter(Sender: TObject);
begin
  imgKo4.Visible := True;
end;

procedure TfrmMainNeiNava.lblKo4MouseLeave(Sender: TObject);
begin
  imgKo4.Visible := False;
end;

procedure TfrmMainNeiNava.lblKo5MouseEnter(Sender: TObject);
begin
  imgKo5.Visible := True;
end;

procedure TfrmMainNeiNava.lblKo5MouseLeave(Sender: TObject);
begin
  imgKo5.Visible := False;
end;

procedure TfrmMainNeiNava.lblKo6MouseEnter(Sender: TObject);
begin
  imgKo6.Visible := True;
end;

procedure TfrmMainNeiNava.lblKo6MouseLeave(Sender: TObject);
begin
  imgKo6.Visible := False;
end;

procedure TfrmMainNeiNava.lblKo7MouseEnter(Sender: TObject);
begin
  imgKo7.Visible := True;
end;

procedure TfrmMainNeiNava.lblKo7MouseLeave(Sender: TObject);
begin
  imgKo7.Visible := False;
end;

procedure TfrmMainNeiNava.lblKo8MouseEnter(Sender: TObject);
begin
  imgKo8.Visible := True;
end;

procedure TfrmMainNeiNava.lblKo8MouseLeave(Sender: TObject);
begin
  imgKo8.Visible := False;
end;

procedure TfrmMainNeiNava.lblKo9MouseEnter(Sender: TObject);
begin
  imgKo9.Visible := True;
end;

procedure TfrmMainNeiNava.lblKo9MouseLeave(Sender: TObject);
begin
  imgKo9.Visible := False;
end;

procedure TfrmMainNeiNava.lblKo10MouseEnter(Sender: TObject);
begin
  imgKo10.Visible := True;
end;

procedure TfrmMainNeiNava.lblKo10MouseLeave(Sender: TObject);
begin
  imgKo10.Visible := False;
end;

procedure TfrmMainNeiNava.lblKo11MouseEnter(Sender: TObject);
begin
  imgKo11.Visible := True;
end;

procedure TfrmMainNeiNava.lblKo11MouseLeave(Sender: TObject);
begin
  imgKo11.Visible := False;
end;

procedure TfrmMainNeiNava.lblKo12MouseEnter(Sender: TObject);
begin
  imgKo12.Visible := True;
end;

procedure TfrmMainNeiNava.lblKo12MouseLeave(Sender: TObject);
begin
  imgKo12.Visible := False;
end;

procedure TfrmMainNeiNava.lblKo13MouseEnter(Sender: TObject);
begin
  imgKo13.Visible := True;
end;

procedure TfrmMainNeiNava.lblKo13MouseLeave(Sender: TObject);
begin
  imgKo13.Visible := False;
end;

procedure TfrmMainNeiNava.lblFk1MouseEnter(Sender: TObject);
begin
  imgFk1.Visible := True;
end;

procedure TfrmMainNeiNava.lblFk1MouseLeave(Sender: TObject);
begin
  imgFk1.Visible := False;
end;

procedure TfrmMainNeiNava.lblFk2MouseEnter(Sender: TObject);
begin
  imgFk2.Visible := True;
end;

procedure TfrmMainNeiNava.lblFk2MouseLeave(Sender: TObject);
begin
  imgFk2.Visible := False;
end;

procedure TfrmMainNeiNava.lblFk3MouseEnter(Sender: TObject);
begin
  imgFk3.Visible := True;
end;

procedure TfrmMainNeiNava.lblFk3MouseLeave(Sender: TObject);
begin
  imgFk3.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv1MouseEnter(Sender: TObject);
begin
  imgAv1.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv1MouseLeave(Sender: TObject);
begin
  imgAv1.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv2MouseEnter(Sender: TObject);
begin
  imgAv2.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv2MouseLeave(Sender: TObject);
begin
  imgAv2.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv3MouseEnter(Sender: TObject);
begin
  imgAv3.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv3MouseLeave(Sender: TObject);
begin
  imgAv3.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv4MouseEnter(Sender: TObject);
begin
  imgAv4.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv4MouseLeave(Sender: TObject);
begin
  imgAv4.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv5MouseEnter(Sender: TObject);
begin
  imgAv5.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv5MouseLeave(Sender: TObject);
begin
  imgAv5.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv6MouseEnter(Sender: TObject);
begin
  imgAv6.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv6MouseLeave(Sender: TObject);
begin
  imgAv6.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv7MouseEnter(Sender: TObject);
begin
  imgAv7.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv7MouseLeave(Sender: TObject);
begin
  imgAv7.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv8MouseEnter(Sender: TObject);
begin
  imgAv8.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv8MouseLeave(Sender: TObject);
begin
  imgAv8.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv9MouseEnter(Sender: TObject);
begin
  imgAv9.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv9MouseLeave(Sender: TObject);
begin
  imgAv9.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv10MouseEnter(Sender: TObject);
begin
  imgAv10.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv10MouseLeave(Sender: TObject);
begin
  imgAv10.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv11MouseEnter(Sender: TObject);
begin
  imgAv11.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv11MouseLeave(Sender: TObject);
begin
  imgAv11.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv12MouseEnter(Sender: TObject);
begin
  imgAv12.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv12MouseLeave(Sender: TObject);
begin
  imgAv12.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv13MouseEnter(Sender: TObject);
begin
  imgAv13.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv13MouseLeave(Sender: TObject);
begin
  imgAv13.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv14MouseEnter(Sender: TObject);
begin
  imgAv14.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv14MouseLeave(Sender: TObject);
begin
  imgAv14.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv15MouseEnter(Sender: TObject);
begin
  imgAv15.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv15MouseLeave(Sender: TObject);
begin
  imgAv15.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv16MouseEnter(Sender: TObject);
begin
  imgAv16.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv16MouseLeave(Sender: TObject);
begin
  imgAv16.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv17MouseEnter(Sender: TObject);
begin
  imgAv17.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv17MouseLeave(Sender: TObject);
begin
  imgAv17.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv18MouseEnter(Sender: TObject);
begin
  imgAv18.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv18MouseLeave(Sender: TObject);
begin
  imgAv18.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv19MouseEnter(Sender: TObject);
begin
  imgAv19.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv19MouseLeave(Sender: TObject);
begin
  imgAv19.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv20MouseEnter(Sender: TObject);
begin
  imgAv20.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv20MouseLeave(Sender: TObject);
begin
  imgAv20.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv21MouseEnter(Sender: TObject);
begin
  imgAv21.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv21MouseLeave(Sender: TObject);
begin
  imgAv21.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv22MouseEnter(Sender: TObject);
begin
  imgAv22.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv22MouseLeave(Sender: TObject);
begin
  imgAv22.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv23MouseEnter(Sender: TObject);
begin
  imgAv23.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv23MouseLeave(Sender: TObject);
begin
  imgAv23.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv24MouseEnter(Sender: TObject);
begin
  imgAv24.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv24MouseLeave(Sender: TObject);
begin
  imgAv24.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv25MouseEnter(Sender: TObject);
begin
  imgAv25.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv25MouseLeave(Sender: TObject);
begin
  imgAv25.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv26MouseEnter(Sender: TObject);
begin
  imgAv26.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv26MouseLeave(Sender: TObject);
begin
  imgAv26.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv27MouseEnter(Sender: TObject);
begin
  imgAv27.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv27MouseLeave(Sender: TObject);
begin
  imgAv27.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv28MouseEnter(Sender: TObject);
begin
  imgAv28.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv28MouseLeave(Sender: TObject);
begin
  imgAv28.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv29MouseEnter(Sender: TObject);
begin
  imgAv29.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv29MouseLeave(Sender: TObject);
begin
  imgAv29.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv30MouseEnter(Sender: TObject);
begin
  imgAv30.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv30MouseLeave(Sender: TObject);
begin
  imgAv30.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv31MouseEnter(Sender: TObject);
begin
  imgAv31.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv31MouseLeave(Sender: TObject);
begin
  imgAv31.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv32MouseEnter(Sender: TObject);
begin
  imgAv32.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv32MouseLeave(Sender: TObject);
begin
  imgAv32.Visible := False;
end;

procedure TfrmMainNeiNava.lblAv33MouseEnter(Sender: TObject);
begin
  imgAv33.Visible := True;
end;

procedure TfrmMainNeiNava.lblAv33MouseLeave(Sender: TObject);
begin
  imgAv33.Visible := False;
end;

procedure TfrmMainNeiNava.lblSo1MouseEnter(Sender: TObject);
begin
  imgSo1.Visible := True;
end;

procedure TfrmMainNeiNava.lblSo1MouseLeave(Sender: TObject);
begin
  imgSo1.Visible := False;
end;

procedure TfrmMainNeiNava.lblSo2MouseEnter(Sender: TObject);
begin
  imgSo2.Visible := True;
end;

procedure TfrmMainNeiNava.lblSo2MouseLeave(Sender: TObject);
begin
  imgSo2.Visible := False;
end;

procedure TfrmMainNeiNava.lblSo3MouseEnter(Sender: TObject);
begin
  imgSo3.Visible := True;
end;

procedure TfrmMainNeiNava.lblSo3MouseLeave(Sender: TObject);
begin
  imgSo3.Visible := False;
end;

procedure TfrmMainNeiNava.lblSo4MouseEnter(Sender: TObject);
begin
  imgSo4.Visible := True;
end;

procedure TfrmMainNeiNava.lblSo4MouseLeave(Sender: TObject);
begin
  imgSo4.Visible := False;
end;

procedure TfrmMainNeiNava.lblSo5MouseEnter(Sender: TObject);
begin
  imgSo5.Visible := True;
end;

procedure TfrmMainNeiNava.lblSo5MouseLeave(Sender: TObject);
begin
  imgSo5.Visible := False;
end;

procedure TfrmMainNeiNava.lblSo6MouseEnter(Sender: TObject);
begin
  imgSo6.Visible := True;
end;

procedure TfrmMainNeiNava.lblSo6MouseLeave(Sender: TObject);
begin
  imgSo6.Visible := False;
end;

procedure TfrmMainNeiNava.lblSo7MouseEnter(Sender: TObject);
begin
  imgSo7.Visible := True;
end;

procedure TfrmMainNeiNava.lblSo7MouseLeave(Sender: TObject);
begin
  imgSo7.Visible := False;
end;

procedure TfrmMainNeiNava.lblSo8MouseEnter(Sender: TObject);
begin
  imgSo8.Visible := True;
end;

procedure TfrmMainNeiNava.lblSo8MouseLeave(Sender: TObject);
begin
  imgSo8.Visible := False;
end;

procedure TfrmMainNeiNava.lblSo9MouseEnter(Sender: TObject);
begin
  imgSo9.Visible := True;
end;

procedure TfrmMainNeiNava.lblSo9MouseLeave(Sender: TObject);
begin
  imgSo9.Visible := False;
end;

procedure TfrmMainNeiNava.lblMu1MouseEnter(Sender: TObject);
begin
  imgMu1.Visible := True;
end;

procedure TfrmMainNeiNava.lblMu1MouseLeave(Sender: TObject);
begin
  imgMu1.Visible := False;
end;

procedure TfrmMainNeiNava.lblMu2MouseEnter(Sender: TObject);
begin
  imgMu2.Visible := True;
end;

procedure TfrmMainNeiNava.lblMu2MouseLeave(Sender: TObject);
begin
  imgMu2.Visible := False;
end;

procedure TfrmMainNeiNava.lblMu3MouseEnter(Sender: TObject);
begin
  imgMu3.Visible := True;
end;

procedure TfrmMainNeiNava.lblMu3MouseLeave(Sender: TObject);
begin
  imgMu3.Visible := False;
end;

procedure TfrmMainNeiNava.lblMu4MouseEnter(Sender: TObject);
begin
  imgMu4.Visible := True;
end;

procedure TfrmMainNeiNava.lblMu4MouseLeave(Sender: TObject);
begin
  imgMu4.Visible := False;
end;

procedure TfrmMainNeiNava.lblMu5MouseEnter(Sender: TObject);
begin
  imgMu5.Visible := True;
end;

procedure TfrmMainNeiNava.lblMu5MouseLeave(Sender: TObject);
begin
  imgMu5.Visible := False;
end;

procedure TfrmMainNeiNava.lblMu6MouseEnter(Sender: TObject);
begin
  imgMu6.Visible := True;
end;

procedure TfrmMainNeiNava.lblMu6MouseLeave(Sender: TObject);
begin
  imgMu6.Visible := False;
end;

procedure TfrmMainNeiNava.lblMu7MouseEnter(Sender: TObject);
begin
  imgMu7.Visible := True;
end;

procedure TfrmMainNeiNava.lblMu7MouseLeave(Sender: TObject);
begin
  imgMu7.Visible := False;
end;

procedure TfrmMainNeiNava.lblMu8MouseEnter(Sender: TObject);
begin
  imgMu8.Visible := True;
end;

procedure TfrmMainNeiNava.lblMu8MouseLeave(Sender: TObject);
begin
  imgMu8.Visible := False;
end;

procedure TfrmMainNeiNava.lblMu9MouseEnter(Sender: TObject);
begin
  imgMu9.Visible := True;
end;

procedure TfrmMainNeiNava.lblMu9MouseLeave(Sender: TObject);
begin
  imgMu9.Visible := False;
end;

procedure TfrmMainNeiNava.lblNa1MouseEnter(Sender: TObject);
begin
  imgNa1.Visible := True;
end;

procedure TfrmMainNeiNava.lblNa1MouseLeave(Sender: TObject);
begin
  imgNa1.Visible := False;
end;

procedure TfrmMainNeiNava.lblNa2MouseEnter(Sender: TObject);
begin
  imgNa2.Visible := True;
end;

procedure TfrmMainNeiNava.lblNa2MouseLeave(Sender: TObject);
begin
  imgNa2.Visible := False;
end;

procedure TfrmMainNeiNava.lblNa3MouseEnter(Sender: TObject);
begin
  imgNa3.Visible := True;
end;

procedure TfrmMainNeiNava.lblNa3MouseLeave(Sender: TObject);
begin
  imgNa3.Visible := False;
end;

procedure TfrmMainNeiNava.lblNa4MouseEnter(Sender: TObject);
begin
  imgNa4.Visible := True;
end;

procedure TfrmMainNeiNava.lblNa4MouseLeave(Sender: TObject);
begin
  imgNa4.Visible := False;
end;

procedure TfrmMainNeiNava.lblNa5MouseEnter(Sender: TObject);
begin
  imgNa5.Visible := True;
end;

procedure TfrmMainNeiNava.lblNa5MouseLeave(Sender: TObject);
begin
  imgNa5.Visible := False;
end;

procedure TfrmMainNeiNava.lblNa6MouseEnter(Sender: TObject);
begin
  imgNa6.Visible := True;
end;

procedure TfrmMainNeiNava.lblNa6MouseLeave(Sender: TObject);
begin
  imgNa6.Visible := False;
end;

procedure TfrmMainNeiNava.lblNa7MouseEnter(Sender: TObject);
begin
  imgNa7.Visible := True;
end;

procedure TfrmMainNeiNava.lblNa7MouseLeave(Sender: TObject);
begin
  imgNa7.Visible := False;
end;

procedure TfrmMainNeiNava.lblNa8MouseEnter(Sender: TObject);
begin
  imgNa8.Visible := True;
end;

procedure TfrmMainNeiNava.lblNa8MouseLeave(Sender: TObject);
begin
  imgNa8.Visible := False;
end;

procedure TfrmMainNeiNava.lblNa9MouseEnter(Sender: TObject);
begin
  imgNa9.Visible := True;
end;

procedure TfrmMainNeiNava.lblNa9MouseLeave(Sender: TObject);
begin
  imgNa9.Visible := False;
end;

procedure TfrmMainNeiNava.lblNa10MouseEnter(Sender: TObject);
begin
  imgNa10.Visible := True;
end;

procedure TfrmMainNeiNava.lblNa10MouseLeave(Sender: TObject);
begin
  imgNa10.Visible := False;
end;

procedure TfrmMainNeiNava.lblNa11MouseEnter(Sender: TObject);
begin
  imgNa11.Visible := True;
end;

procedure TfrmMainNeiNava.lblNa11MouseLeave(Sender: TObject);
begin
  imgNa11.Visible := False;
end;

procedure TfrmMainNeiNava.lblNa12MouseEnter(Sender: TObject);
begin
  imgNa12.Visible := True;
end;

procedure TfrmMainNeiNava.lblNa12MouseLeave(Sender: TObject);
begin
  imgNa12.Visible := False;
end;

procedure TfrmMainNeiNava.lblNa13MouseEnter(Sender: TObject);
begin
  imgNa13.Visible := True;
end;

procedure TfrmMainNeiNava.lblNa13MouseLeave(Sender: TObject);
begin
  imgNa13.Visible := False;
end;

procedure TfrmMainNeiNava.lblNa14MouseEnter(Sender: TObject);
begin
  imgNa14.Visible := True;
end;

procedure TfrmMainNeiNava.lblNa14MouseLeave(Sender: TObject);
begin
  imgNa14.Visible := False;
end;

procedure TfrmMainNeiNava.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose := False;
  try
    Timer1.Enabled := False;
    if MediaPlayer1.Mode = mpPlaying then
      MediaPlayer1.Pause;
  except
    ;
  end;
  frmExit.ShowModal;
end;

procedure TfrmMainNeiNava.ChangeSound(v: Smallint);
begin
  if v < 0 then
    v := 0
  else if v > 255 then
    v := 255;
  Vol := v;
  SoundHeightShow;
  if not Mute then
    mVolControl1.Volume := Vol;
end;

procedure TfrmMainNeiNava.lblChangeMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  h: byte;
begin
  with lblChange do
  begin
    if (X in [0..Width]) and
      (Y in [0..Height]) then
    begin
      h := ((Height - Y) * 255) div Height;
      ChangeSound(h);
    end;
  end;
end;

end.
